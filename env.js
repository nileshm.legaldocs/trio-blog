module.exports = {
    env:'local',
    ROOT:'http://localhost:3002/',
    HOST:'http://localhost:3002',
    testEmail:'nilemandal@yahoo.com',
    port:3002,
    ip:'127.0.0.1',
    db:{
        host:'localhost',
        user:'root',
        password:'',
        database:'trio-blog',
        port:'3306',
    }
}
module.exports = (imp) => {
    var router = imp.router;
    
    router.get('/login',(req,res)=>{
        if(!req.session.admin){
            res.render('login');
        }else{
            res.redirect('/home');
        }
    });


}
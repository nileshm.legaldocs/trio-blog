var express = require('express');
var router = express.Router();
var Pool = require('../models/pool');
var env = require('../env');
var pool = new Pool(env.db.host,env.db.user,env.db.password,env.db.database);

var exp = {
    'router':router,
    'pool':pool
}

require('./home')(exp);
require('./admin')(exp);
require('./login')(exp);
require('./about')(exp);

module.exports = router;

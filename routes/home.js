module.exports = (exp) => {
    var router = exp.router;
    var pool = exp.pool;

    router.get('/home',async (req,res)=>{
        let name = (req.session.admin)?req.session.admin.name:'';
        let admin = (await pool.queryAsync(`SELECT * FROM admin where name = ${name}`))[0];
        if(req.session.admin && req.session.admin.aid){
            let data = {
                'name':req.session.admin.name,
                'phone':req.session.admin.phone,
            }
            res.render('index',{data});
        }else{
            res.redirect('/login');
        }
    });
}
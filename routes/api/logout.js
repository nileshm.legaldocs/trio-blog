module.exports = (exp) => {
    var router = exp.router;
    
    router.get('/logout', (req,res)=>{
        if(req.session.cookie && req.session.admin){
            req.session.destroy(()=>{
                res.redirect('/home');
            });
        }else{
            res.send('already session is empty');
        }
    })
}
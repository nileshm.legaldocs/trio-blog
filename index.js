const express = require('express');
var app = express();
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');

var session = require('express-session')
var sqlSession = require('express-mysql-session')(session);
var sessionStore = new sqlSession({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'trio-blog'
})
var port = 3000;

/**
 * USER DEFINE MODEL
 * pool model to get the DB data from mysql
 */
let host = 'localhost';
let user = 'root';
let password = '';
let database = 'trio-blog';
var Pool = require('./models/pool');
var pool = new Pool(host,user,password,database);

/* Template Engine set */
app.engine('hbs', expressHbs({
    extname:'hbs',
    defaultLayout:'layout',
    layoutsDir:__dirname+'/views/layouts/',
    partialsDir:__dirname+'/views/partials/'
}));
app.set('view engine','hbs');

/* Static Directory for static files storages */
app.use(express.static('public'));

/**
 * SESSION STORAGE
 */
app.use(session({secret:'anySecret',store: sessionStore, saveUninitialized: true,resave: true}));

/**
 * Body Parse use for the req.body to get the body data for post Request
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

/**
 * LandingPage of website
 */
app.get('/', (req,res) => {
    res.send('welcome to the Node World <a href="home">home</a>');
})

/**
 * Routes of web page
 */
app.use('/',require('./routes/landingPage'));
app.use('/api',require('./routes/api/_api'));

// app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
app.listen(port);
var mysql = require('mysql');
var poolArr={};

module.exports = function Pool(host,user,password,database){
    var pool;

    if(poolArr[user+'|'+database]){
        pool=poolArr[user+'|'+database];
    }else{
        pool = mysql.createPool({
            host:host,
            user:user,
            password:password,
            database:database,
            connectionLimit:20
        });

        pool.getConnection((err,connection)=>{
            if (err) {
                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.error('Database connection was closed.')
                }
                if (err.code === 'ER_CON_COUNT_ERROR') {
                    console.error('Database has too many connections.')
                }
                if (err.code === 'ECONNREFUSED') {
                    console.error('Database connection was refused.')
                }
            }
            if(connection) connection.release()
            return
        });

        pool.queryAsync = (query) =>{
            return new Promise((resolve,reject) => {
                pool.getConnection( (err,connection) => {
                    connection.query(query, (error , results, fileds) => {
                        connection.release();
                        if(error){
                            resolve([]);
                            return;
                        }
                        if(results.length==0){
                            results.push({})
                        }
                        resolve(results);
                    })
                });
            });
        }
    
        poolArr[user+'|'+database]=pool;

    }
    return pool;
}